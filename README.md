# M2Crypto Installation process

M2Crypto used in loansingh project to generate encrypted signature and payload for perfios API communication

Steps to install M2Crypto-0.25.1
1. Extract Zip folder tar -xzf M2Crypto-0.25.1.tar.gz
2. cd M2Crypto-0.25.1 which contain SWIG directory
3. In SWIG you'll find _ssl.i, which is the file to be patched.
4. In same folder i placed a patch file.
5. cd SWIG. 
6. Inside SWIG run patch command:- patch _ssl.i _ssl.i.patch
7. After patch back to M2Crypto-0.25.1 folder
8. Run python setup.py build and then python setup.py install
9. To test this installation go to python shell and import M2Crypto
10.Thank you
